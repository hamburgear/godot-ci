# Godot graphical Continuous Integration using Gitlab & Ubuntu
by Hamburgear
#### Youtube Video Series: [Godot graphical CI using Gitlab & Ubuntu](https://youtu.be/QUWzszp56zw)

You may ask why do we need continuous integration for game development?

One benefit of it is the rapid feedback, making us sure that
every time we add or modify our project we will know early if 
our new codes or modifications does not break our game.

It is an added item in our game development workflow but its benefit will help us focus on progressing our game than finding bugs that we may introduce.

Character Sprite: [Jungle Pack](https://jesse-m.itch.io/jungle-pack) by Jesse Munguia
