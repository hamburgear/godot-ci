extends KinematicBody2D

var speed: int = 100
var move_direction: Vector2 = Vector2(0, 0)
var motion: Vector2
var is_moving: bool

func _init():
	move_direction = Vector2(0, 0)
	is_moving = false

func _process(delta):
	movement_input()

func _physics_process(delta):
	if is_moving:
		movement_update()
	
func movement_input():
	move_direction.x = int(Input.is_action_pressed("right")) - int(Input.is_action_pressed("left"))
	move_direction.y = int(Input.is_action_pressed("down")) - int(Input.is_action_pressed("up"))
	
	motion = move_direction.normalized() * speed
	
	if (motion == Vector2.ZERO):
		is_moving = false
		self.get_node("AnimatedSprite")._set_playing(false)
	else:
		is_moving = true
		self.get_node("AnimatedSprite")._set_playing(true)
	
func movement_update():
	move_and_slide(motion)
