extends Node

var root_node: Node
var stop_moving_timer: Timer
var key: InputEventAction
var is_inputting: bool
var run_time: float = 1.00
var initial_position: Vector2 = Vector2(400, 400)
var is_current_test_finished: bool = true
var current_unit_test: int = 0
var unit_tests: Array = [
	"move_right",
	"move_down",
	"move_left",
	"move_up"
]

func _init():
	print("Movement test script loaded.")
	is_inputting = false
	key = InputEventAction.new()
	
func run_test():
	set_process(true)
	root_node = get_parent()
	root_node.get_node("SceneRoot/Character").position = initial_position
	is_inputting = true
	self.call_deferred("setup_stop_moving_timer")

func _process(delta):
	if is_current_test_finished and current_unit_test < unit_tests.size():
		is_current_test_finished = false
		print("Unit Test: ", unit_tests[current_unit_test])
		self.call_deferred(unit_tests[current_unit_test])
	
	if current_unit_test == unit_tests.size():
		stop_moving_timer.stop()
		print("All Tests Finished")
		set_process(false)

func move_right():
	key.action = "right"
	key.pressed = true
	Input.parse_input_event(key)

func move_left():
	key.action = "left"
	key.pressed = true
	Input.parse_input_event(key)

func move_up():
	key.action = "up"
	key.pressed = true
	Input.parse_input_event(key)
	
func move_down():
	key.action = "down"
	key.pressed = true
	Input.parse_input_event(key)

func setup_stop_moving_timer():
	stop_moving_timer = Timer.new()
	self.add_child(stop_moving_timer)
	stop_moving_timer.connect("timeout", self, "stop_moving_input")
	stop_moving_timer.start(run_time)
	

func stop_moving_input():
	is_inputting = false
	key.pressed = false
	Input.parse_input_event(key)
	
	self.verify_test(current_unit_test)
	
	root_node.get_node("SceneRoot/Character").position = initial_position
	
	current_unit_test += 1
	is_current_test_finished = true

func verify_test(current_unit_test):
	self.call("verify_" + unit_tests[current_unit_test])

func verify_move_right():
	var character: KinematicBody2D = self.get_parent().get_node("SceneRoot/Character")
	if character.position.x > initial_position.x and character.position.y == initial_position.y:
		print("Move Right: Success")
	else:
		print("Move Right: Failed")

func verify_move_left():
	var character: KinematicBody2D = self.get_parent().get_node("SceneRoot/Character")
	if character.position.x < initial_position.x and character.position.y == initial_position.y:
		print("Move Left: Success")
	else:
		print("Move Left: Failed")

func verify_move_up():
	var character: KinematicBody2D = self.get_parent().get_node("SceneRoot/Character")
	if character.position.x == initial_position.x and character.position.y < initial_position.y:
		print("Move Up: Success")
	else:
		print("Move Up: Failed")

func verify_move_down():
	var character: KinematicBody2D = self.get_parent().get_node("SceneRoot/Character")
	if character.position.x == initial_position.x and character.position.y > initial_position.y:
		print("Move Down: Success")
	else:
		print("Move Down: Failed")
