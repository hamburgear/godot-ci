extends Node

var commit_hash_path: String = "/home/burger/workspace/commithash.txt"
var test_flow_path: String = "/home/burger/workspace/test_flow.json"
var pcks_folder: String = "/home/burger/workspace/build"

var current_commit_hash: String = ""
var commit_hash_file_timer: Timer
var commit_hash_file_timer_label: Label

var scene_root: Node
var test_flow: Dictionary = {}
var is_loading_finished: bool
var ERROR_TAG: String = "E - "
var INFO_TAG: String = "I - "

var test_script_node: Node
var test_script: Script

func _ready():
	is_loading_finished = true
	commit_hash_file_timer = $CommitHashFileTimer
	commit_hash_file_timer_label = $RootUI/CommitHashTimerLabel
	test_script_node = $TestScriptNode
	scene_root = $SceneRoot

func _process(delta):
	commit_hash_file_timer_label.text = "Next commit check: " + str(int(commit_hash_file_timer.time_left))
	
func _on_CommitHashFileTimer_timeout():
	if is_loading_finished:
		is_loading_finished = false
		read_commit_hash_file()
		
func read_commit_hash_file():
	print(INFO_TAG, get_timestamp(), "Scan for new commit hash file.")
	var dir = Directory.new()
	if dir.file_exists(commit_hash_path):
		var file = File.new()
		file.open(commit_hash_path, File.READ)
		var commit_hash = file.get_as_text().strip_edges()
		print(INFO_TAG, get_timestamp(), "New commit hash file detected.")
		if current_commit_hash != commit_hash:
			load_test_flow()
			load_pck_list()
			execute_phases()
			current_commit_hash = commit_hash
		file.close()
		dir.remove(commit_hash_path)
	else:
		print(INFO_TAG, get_timestamp(), "No new commit.")
	
	is_loading_finished = true

func load_test_flow():
	print(INFO_TAG, get_timestamp(), "Load test flow.")
	var file = File.new()
	file.open(test_flow_path, File.READ)
	test_flow = parse_json(file.get_as_text())
	file.close()
	print(INFO_TAG, get_timestamp(), "Test flow loaded.")

func load_pck_list():
	print(INFO_TAG, get_timestamp(), "Load packs.")
	if test_flow["content"].has("packs"):
		var packs = test_flow["content"]["packs"]
		var has_error: bool = false
		
		for pack in packs:
			var pack_path: String = pcks_folder + "/" + pack + ".pck"
			print(INFO_TAG, get_timestamp(), "Loading " + pack + "...")
			var is_pck_loaded_successfully: bool = ProjectSettings.load_resource_pack(pack_path)
			if !is_pck_loaded_successfully:
				has_error = true
				print(ERROR_TAG, get_timestamp(), "Error loading loading Pack: ", PacketPeerUDP)
				continue
				
			print(INFO_TAG, get_timestamp(), "Successfully loaded ", pack)
			
		if !has_error:
			print(INFO_TAG, get_timestamp(), "Successfully loaded all packs.")

func execute_phases():
	print(INFO_TAG, get_timestamp(), "Load Test Phases.")
	if test_flow["content"].has("phases"):
		var phases = test_flow["content"]["phases"]
		for phase in phases:
			var phase_package: Dictionary = phase
			if phase_package.has("scene"):
				var script_name: String = phase_package["script"]
				test_script = load("res://Scripts/" + script_name + ".gd")
				
			if phase_package.has("type"):
				var type: String = phase_package["type"]
				var packed_scene: PackedScene = load("res://" + type + "/" + phase_package["scene"] + ".tscn")
				var scene: Node = packed_scene.instance()
				scene_root.call_deferred("add_scene_to_test", scene)

func initialize_test_script():
	test_script_node.set_script(test_script)
	test_script_node.run_test()

func get_timestamp():
	var timestamp: Dictionary = OS.get_time()
	return str("[", timestamp["hour"], ":", timestamp["minute"], ":", timestamp["second"], "] ")
