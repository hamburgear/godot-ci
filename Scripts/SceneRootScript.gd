extends Node

var root_node: Node

func add_scene_to_test(scene: Node):
	root_node = get_parent()
	self.add_child(scene)
	root_node.initialize_test_script()
